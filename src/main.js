import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import './index.css'
import {createStore} from 'vuex'
import axios from 'axios'
import user from './store/user'

axios.defaults.baseURL = 'http://localhost:9090'

const store = createStore({
    modules: {
        user
    }
})

const app = createApp(App)
app.use(router)
app.mount('#app')
app.use(store)

// init store
axios.get('/api/v1/auth/me').then(response => {
    console.log(response.data)
    store.commit('UPDATE_USER', response.data.user)
})
