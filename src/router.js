import {createRouter, createWebHistory} from "vue-router";
import Dashboard from './components/dashboard.vue'
import AuthComponent from './components/auth/auth.vue'
import Welcome from './components/home/welcome.vue'
import Settings from './components/home/settings.vue'
import Help from './components/home/help.vue'
import PasswordManagers from './components/home/password-managers.vue'
import ApplicationReview from './components/application/review.vue'
import LoginComponent from './components/auth/login.vue'
import RegisterComponent from './components/auth/register.vue'

const routes = [
    {
        path: '/dashboard',
        component: Dashboard,
        children: [
            {
                path: '/home/welcome',
                name: 'dashboard-home-welcome',
                component: Welcome,
            },
            {
                path: '/home/settings',
                name: 'dashboard-home-settings',
                component: Settings,
            },
            {
                path: '/home/help',
                name: 'dashboard-home-help',
                component: Help,
            },
            {
                path: '/home/password-managers',
                name: 'dashboard-home-password-managers',
                component: PasswordManagers,
            },
            {
                path: '/application/review',
                name: 'dashboard-application-review',
                component: ApplicationReview,
            },
            {
                path: '/home/settings',
                name: 'dashboard-settings',
                component: Settings
            }
        ]
    },
    {
        path: '/auth',
        component: AuthComponent,
        children: [
            {
                path: '/auth/login',
                name: 'auth-login',
                component: LoginComponent,
            },
            {
                path: '/auth/register',
                name: 'auth-register',
                component: RegisterComponent,
            },
        ]
    }
]

const router = createRouter({
    history: createWebHistory(),
    routes,
})

function isAuthenticated() {
    const token = localStorage.getItem('dhamma_token')
    return token !== null
}

router.beforeResolve((to, from, next) => {
    if(to.name !== 'auth-login' && !isAuthenticated()) return next({ name: 'auth-login' })
    if(to.name === 'auth-login' && isAuthenticated()) return next({ name: 'dashboard-home-welcome' })
    else if(to.path === '/') return next({ name: 'dashboard-home-welcome' })
    else next()
})

export default router
